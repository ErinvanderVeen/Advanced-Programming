module assignment11

from StdFunc import id, o
from StdMisc import undef
import StdBool
import StdEnum
import StdInt
import StdList
import StdOverloaded
import StdString

from Data.Functor import class Functor(fmap)
from Text import class Text(concat), instance Text String
import Data.Error
import Data.List
import Data.Maybe
import Data.Tuple
import qualified Data.Map as Map

:: Ident :== String

// TYPES
class Type a | toString, TC a
where
	type :: a -> String

instance Type ()     where type _ = "()"
instance Type Int    where type _ = "Int"
instance Type Char   where type _ = "Char"
instance Type Bool   where type _ = "Bool"

// UPD, EXPR, STMT
:: Upd  = Upd
:: Expr = Expr
:: Stmt = Stmt

:: In a b = (In) infix 0 a b
:: Do a b = (Do) infix 0 a b

class isStmt a :: (a -> a)
instance isStmt Stmt where isStmt = id
instance isStmt Expr where isStmt = id
instance isStmt Upd  where isStmt = id

class isExpr a :: (a -> a)
instance isExpr Expr where isExpr = id
instance isExpr Upd  where isExpr = id

// EXPRESSIONS
class aExpr v
where
	lit :: a -> v a Expr | Type a
	(+.) infixl 6 :: (v a p) (v a q) -> v a Expr | +, Type a
	(-.) infixl 6 :: (v a p) (v a q) -> v a Expr | -, Type a
	(*.) infixl 7 :: (v a p) (v a q) -> v a Expr | *, Type a
	(~.) :: (v a p) -> v a Expr                  | ~, Type a

class bExpr v
where
	(&&.) infixr 3 :: (v Bool p) (v Bool q) -> v Bool Expr
	(||.) infixr 2 :: (v Bool p) (v Bool q) -> v Bool Expr
	(==.) infix  4 :: (v a p)    (v a q)    -> v Bool Expr | ==, Type a
	(<.)  infix  4 :: (v a p)    (v a q)    -> v Bool Expr | <,  Type a

// Location of the : defines the side of the set
class sExpr v
where
	//new :: [a] -> v [a] Expr | Type a
	new :: [Int] -> v [Int] Expr
	(+:) infix 5  :: (v a p) (v [a] q) -> v [a] Expr   | Type, Eq a
	(:+) infix 5  :: (v [a] p) (v a q) -> v [a] Expr   | Type, Eq a
	(:+:) infix 6 :: (v [a] p) (v [a] q) -> v [a] Expr | Type, Eq a
	(:-) infix 5  :: (v [a] p) (v a q) -> v [a] Expr   | Type, Eq a
	(:-:) infix 6 :: (v [a] p) (v [a] q) -> v [a] Expr | Type, Eq a
	(:^:) infix 7 :: (v [a] p) (v [a] q) -> v [a] Expr | Type, Eq a

class var v
where
	(=.) infixr 2 :: (v a Upd) (v a p) -> v a p | Type a & isExpr p
	var :: ((v a Upd) -> In a (v b p)) -> v b p | Type a

class Stmt v
where
	If :: (v Bool p) (v a q) (v b r) -> v () Stmt | isExpr p
	For :: ((v t Upd) -> Do (v [t] Expr) (v u a)) -> v () Stmt | Type t
	(:.) infix 1 :: (v a p) (v b q) -> v b Stmt

// PRINTING
:: Show a p = Show (SHOW -> SHOW)
:: SHOW =
	{ fresh :: Int
	, print :: [String]
	}

s0 :: SHOW
s0 =
	{ fresh = 0
	, print = []
	}

print :: (Show s t) -> String
print (Show s) = concat (s s0).SHOW.print

unShow :: (Show a b) -> (SHOW -> SHOW)
unShow (Show f) = f

showSet :: [a] -> Show b c | toString a
showSet xs = c "[" +.+ showSet` (map c xs)
where
	showSet` :: [Show a b] -> Show c d
	showSet` [] = Show id
	showSet` [x] = x +.+ c "]"
	showSet` [x : xs] = x +.+ c ", " +.+ (showSet` xs)

// From Slides
c :: a -> Show v c | toString a
c a = Show \c -> {c & print = [toString a : c.print]}

(+.+) infixl 5 :: (Show a p) (Show b q) -> Show c r
(+.+) (Show f) (Show g) = Show (f o g)

parens :: String String (Show a p) -> Show b q
parens l r e = c l +.+ e +.+ c r

brac :: (Show a p) -> Show b q
brac e = parens "(" ")" e

fresh :: (Int -> Show a p) -> Show a p
fresh f = Show \c -> unShow (f c.fresh) {c & fresh = c.fresh + 1}

freshVar :: ((Show b q) -> (Show a p)) -> Show a p
freshVar f = fresh (f o \n -> c ("v" +++ toString n))
//

// SHOW
instance aExpr Show
where
	lit a = c a
	(+.) x y = brac (x +.+ c " + " +.+ y)
	(-.) x y = brac (x +.+ c " - " +.+ y)
	(*.) x y = brac (x +.+ c " * " +.+ y)
	(~.) x = c " ~ " +.+ brac x

instance bExpr Show
where
	(&&.) x y = brac (x +.+ c " &&. " +.+ y)
	(||.) x y = brac (x +.+ c " ||. " +.+ y)
	(==.) x y = brac (x +.+ c " ==. " +.+ y)
	(<.)  x y = brac (x +.+ c " <. "  +.+ y)

instance sExpr Show
where
	new x = showSet x
	(+:)  x y = brac (x +.+ c " +: " +.+ y)
	(:+)  x y = brac (x +.+ c " :+ " +.+ y)
	(:+:) x y = brac (x +.+ c " :+: " +.+ y)
	(:-)  x y = brac (x +.+ c " :- " +.+ y)
	(:-:) x y = brac (x +.+ c " :-: " +.+ y)
	(:^:) x y = brac (x +.+ c " :^: " +.+ y)

instance var Show
where
	(=.) v e = v +.+ c " = " +.+ e
	var f = freshVar \v -> let (x In rest) = f v in
		c (type x) +.+ c " " +.+ v +.+ c " = " +.+ c x +.+ c ";" +.+ rest

instance Stmt Show
where
	If b t e = c "if " +.+ b +.+ c " {" +.+ t +.+ c "} else {" +.+ e +.+ c "}"
	For f = freshVar \v -> let (x Do rest) = f v in
		c "For "+.+ v +.+ c " in " +.+ x +.+ c " Do {" +.+ rest +.+ c "}"
	(:.) s t = s +.+ c ";" +.+ t

// EVAL
// Eval () Stmt
// Eval ((RW ()) State -> MaybeError String ((), State))
:: Eval t s = Eval ((RW t) State -> MaybeError String (t, State))
:: RW t = R | W t
:: State =
	{ vars :: 'Map'.Map Int Dynamic
	, next :: Int
	}

eval :: (Eval t s) -> MaybeError String (t, State)
eval (Eval x) = x R {vars = 'Map'.newMap, next = 1}

(<$>) infixl 4 :: (a -> b) (Eval a c) -> Eval b d
(<$>) f (Eval x) = Eval \_ st -> fmap (appFst f) (x R st)

pure :: a -> Eval a t
pure x = Eval \_ st -> Ok (x, st)

unEval :: (Eval t s) -> ((RW t) State -> MaybeError String (t, State))
unEval (Eval f) = f

(>>-) infixl 1 :: (Eval a p) (a -> Eval b q) -> Eval b q
(>>-) (Eval e) f =
	Eval \r s -> case e R s of
		Ok (a, st) = unEval (f a) r st
		Error msg = Error msg

liftA2 :: (a b -> c) (Eval a t) (Eval b u) -> Eval c v
liftA2 f (Eval x) (Eval y) = Eval \_ st -> case x R st of
	Ok (x, st) = case y R st of
		Ok (y, st) = Ok (f x y, st)
		Error e = Error e
	Error e = Error e

instance aExpr Eval
where
	lit x = pure x
	(+.) x y = liftA2 (+) x y
	(-.) x y = liftA2 (-) x y
	(*.) x y = liftA2 (*) x y
	(~.) x = (~) <$> x

instance bExpr Eval
where
	(&&.) x y = liftA2 (&&) x y
	(||.) x y = liftA2 (||) x y
	(==.) x y = liftA2 (==) x y
	(<.)  x y = liftA2 (<)  x y

insert :: a [a] -> [a] | Eq a
insert x xs
| isMember x xs = xs
= [x : xs]

instance sExpr Eval
where
	new x = pure x
	(+:)  x y = liftA2 insert x y
	(:+)  x y = liftA2 insert y x
	(:+:) x y = liftA2 union x y
	(:-)  x y = liftA2 removeMember y x
	(:-:) x y = liftA2 difference x y
	(:^:) x y = liftA2 intersect x y

rwvar :: Int (RW a) (State) -> MaybeError String (a, State) | TC a
rwvar i R st = case 'Map'.get i st.vars of
	Just (v :: a^) -> Ok (v, st)
	Just _         -> Error "Wrong type"
	Nothing        -> Error "Undefined variable"
rwvar i (W x) st = Ok (x, {st & vars='Map'.put i (dynamic x) st.vars})

instance var Eval
where
	(=.) v e = e >>- \a -> Eval \r s -> unEval v (W a) s
	var f = Eval \r s -> let (x In (Eval rest)) = f (Eval (rwvar s.next)) in
		rest R { s & next = s.next + 1
		       , vars = 'Map'.put s.next (dynamic x) s.vars
		       }

toStmt :: (Eval t p) -> Eval t Stmt
toStmt (Eval f) = Eval f

instance Stmt Eval where
	(:.) s t = s >>- \_ -> toStmt t
	If b t e = b >>- \c -> if c (t >>- \_ -> pure ()) (e >>- \_ -> pure ())
	For f = Eval \_ st -> let (xs Do b) = f (Eval (rwvar st.next)) in
		let (Eval e) = For` st.next xs b in e R {st & next = st.next + 1}
	where
		For` :: Int (Eval [a] Expr) (Eval b t) -> Eval () Stmt | TC a
		For` i s (Eval b) = s >>- \s -> Eval (\rw st -> iteration i s rw st)
		where
			iteration :: Int [a] (RW t) State -> MaybeError String ((), State) | TC a
			iteration i []     _ st = Ok ((), st)
			iteration i [x:xs] a st = case b R {st & vars = 'Map'.put i (dynamic x) st.vars} of
				Ok (_, st) = iteration i xs a st
				Error e    = Error e

Start = eval stmt
where
	stmt :: Eval Int Stmt
	stmt =
		var \y = 1 In (
			For \x . new [1..10] Do
				y =. y *. x
		):.
		y
