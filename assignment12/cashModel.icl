implementation module cashModel
/*
   Pieter Koopman, Radboud University, 2017
   pieter@cs.ru.nl
   Advanced programming

   A simple state model for an automated cash register
 */

import StdEnv, GenEq
import gastje

class euro a :: a -> Euro

instance euro Product
where
	euro Pizza = euro (4,99)
	euro Beer  = euro (0,65)
	euro _     = euro 1

instance euro Int where euro e = {euro = e, cent = 0}
instance euro (Int, Int) where euro (e,c) = {euro = e, cent = c}
instance euro [e] | euro e where euro l = sum (map euro l)
instance euro Euro where euro e = e

instance + Euro
where
	+ x y = {euro = c / 100, cent = (abs c) rem 100}
	where
		c = (x.euro + y.euro) * 100 + sign x.euro * x.cent + sign y.euro * y.cent

instance - Euro
where
	- x y = {euro = c / 100, cent = (abs c) rem 100}
	where
		c = (x.euro - y.euro) * 100 + sign x.euro * x.cent - sign y.euro * y.cent

instance zero Euro where zero = {euro = 0, cent = 0}
instance == Product where (==) p q = p === q
instance == Euro where (==) p q = p === q
instance ~ Euro where ~ e = {e & euro = ~e.euro}

derive gEq Euro, Product
derive gen Euro, Product, []
derive string Euro, Product, []
derive bimap []

model :: [Product] Action -> ([Product],[Euro])
model list (Add p) = ([p:list],[euro p])
model list (Rem p)
	| isMember p list = (removeMember p list,[~ (euro p)])
	= (list,[])
model list Pay = ([],[euro list])

////////////////////////////////////////
Start = [ ["plus_zero_is_identity       " : test p_plus_zero_is_identity]
        , ["plus_is_associative         " : test p_plus_is_associative]
        , ["minus_identity_is_zero      " : test p_minus_identity_is_zero]
        , ["minus_zero_is_identity      " : test p_minus_zero_is_identity]
        , ["double_negation_is_identity " : test p_double_negation_is_identity]
        , ["negation_zero_is_identity   " : test p_negation_zero_is_identity]
        , ["neg_distributes_over_plus:  " : test p_neg_distributes_over_plus]
        , ["rem_is_fair:                " : test p_rem_is_fair]
        ]
where
	p_plus_zero_is_identity :: Euro -> Equals
	p_plus_zero_is_identity x = x + zero =.= x

	p_plus_is_associative :: Euro Euro Euro -> Equals
	p_plus_is_associative x y z = (x + y) + z =.= x + (y + z)

	p_minus_identity_is_zero :: Euro -> Equals
	p_minus_identity_is_zero x = x - x =.= zero

	p_minus_zero_is_identity :: Euro -> Equals
	p_minus_zero_is_identity x = x - zero =.= x

	p_double_negation_is_identity :: Euro -> Equals
	p_double_negation_is_identity x = ~(~x) =.= x

	p_negation_zero_is_identity :: Equals
	p_negation_zero_is_identity = ~zero_eur =.= zero_eur
	where
		zero_eur :: Euro
		zero_eur = zero

	p_neg_distributes_over_plus :: Euro Euro -> Equals
	p_neg_distributes_over_plus x y = ~(x + y) =.= ~x + ~y

	p_rem_is_fair :: [Product] Product -> Equals
	p_rem_is_fair ps p
	# (nps, eur) = model ps (Rem p)
	= euro nps =.= euro ps + euro eur

/////////////////////////////////////////////
// RESULT 4:
/*
plus_zero_is_identity       Fail for: {Euro|euro = 0  cent = 1 } {Euro|euro = 0  cent = 0 } does not equal {Euro|euro = 0  cent = 1 }
plus_is_associative         Fail for: {Euro|euro = 1  cent = -1 } {Euro|euro = 0  cent = 0 } {Euro|euro = 0  cent = 0 } {Euro|euro = 0  cent = 0 } does not equal {Euro|euro = 0  cent = 99 }
minus_identity_is_zero      Passed
minus_zero_is_identity      Fail for: {Euro|euro = 0  cent = 1 } {Euro|euro = 0  cent = 0 } does not equal {Euro|euro = 0  cent = 1 }
double_negation_is_identity Passed
negation_zero_is_identity   Proof
neg_distributes_over_plus:  Fail for: {Euro|euro = 1  cent = 0 } {Euro|euro = -9223372036854775808  cent = 1 } {Euro|euro = 0  cent = 99 } does not equal {Euro|euro = -1  cent = 1 }
*/

// RESULT 5
/*
rem_is_fair:                Passed
*/
