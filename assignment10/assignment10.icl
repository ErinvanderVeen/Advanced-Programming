/* Erin van der Veen | s4431200
 * Oussama Danba | s4435591
 */
module assignment10

import StdOverloaded
import StdInt
import StdBool
import StdList
import StdString
import StdGeneric
import StdEnum
from StdFunc import o

import Control.Applicative
import Control.Monad
import Control.Monad.State
import Data.Error
import Data.Functor
import qualified Data.Map as Map
import Data.Maybe

:: Set :== Expression [Int]
:: Elem :== Expression Int
:: Logical :== Expression Bool
:: Ident :== String

// State
:: State_ :== 'Map'.Map Ident Dynamic

// :: Sem Dynamic :== StateT (('Map'.Map Ident Dynamic) -> MaybeError String (Dynamic, 'Map'.Map Ident Dynamic))
:: Sem a :== StateT State_ (MaybeError String) a

store :: Ident a -> Sem a | TC a
store id val = StateT (\s -> Ok (val, ('Map'.put id (dynamic val) s)))

read :: Ident -> Sem a | TC a
read id = gets ('Map'.get id) >>= \val -> case val of
	Just (v :: a^) = pure v
	Just _ = fail "Given Type different from Expected Type"
	Nothing = fail "Variable not found"

fail :: String -> Sem a
fail msg = StateT (\_ -> Error msg)

instance + [a] | Eq a where + xs ys = removeDup (xs ++ ys)
instance - [a] | Eq a where - xs ys = removeMembers xs ys
instance * [a] | Eq a where * xs ys = [x \\ x <- xs | isMember x ys]

:: Expression a
	// Expression
	=      New           (Bimap a [Int]) [Int]
	|      Elem          (Bimap a Int)   Int
	|      Variable                      Ident
	|      Size          (Bimap a Int)   Set
	|      (+.) infixl 6                 (Expression a) (Expression a) & + a
	|      (-.) infixl 6                 (Expression a) (Expression a) & - a
	|      (*.) infixl 7                 (Expression a) (Expression a) & * a
	|      (=.) infixl 2                 Ident (Expression a)
	|      Eps           (Bimap a [Int]) Elem Set
	|      Spe           (Bimap a [Int]) Set Elem
	|      Sme           (Bimap a [Int]) Set Elem
	|      Ets           (Bimap a [Int]) Elem Set
	// Logical
	|      TRUE          (Bimap a Bool)
	|      FALSE         (Bimap a Bool)
	|      In            (Bimap a Bool) Elem Set
	| E.b: Eq            (Bimap a Bool) (Expression b) (Expression b) & TC, == b
	| E.b: Leq           (Bimap a Bool) (Expression b) (Expression b) & TC, Ord b
	|      Not           (Bimap a Bool) (Expression Bool)
	|      Or            (Bimap a Bool) (Expression Bool) (Expression Bool)
	|      And           (Bimap a Bool) (Expression Bool) (Expression Bool)
	// Stmt
	|      If                           (Expression Bool) (Expression a) (Expression a)
	|      For                          Ident Set (Expression a)
	       // The result of the first expression can be either an Element or a Set
	| E.b: (:.) infixl 1                (Expression b) (Expression a) & TC b

eval :: (Expression a) -> Sem a | TC a
eval (New bm is) = pure (bm.map_from is)
eval (Elem bm i) = pure (bm.map_from i)
eval (Variable s) = read s
eval (Size bm set) = fmap (bm.map_from o length) (eval set)
eval (+. a b) = liftA2 (+) (eval a) (eval b)
eval (-. a b) = liftA2 (-) (eval a) (eval b)
eval (*. a b) = liftA2 (*) (eval a) (eval b)
eval (=. s a) = eval a >>= store s
eval (Eps bm i is) = eval is >>= \set -> eval i >>= \n -> (pure o bm.map_from) (removeDup [n : set])
eval (Spe bm is i) = eval (Eps bm i is)
eval (Sme bm is i) = eval is >>= \set -> eval i >>= \n -> (pure o bm.map_from) (removeMember n set)
eval (Ets bm i is) = eval is >>= \set -> eval i >>= \n -> (pure o bm.map_from) (map ((*) n) set)
eval (TRUE bm) = pure (bm.map_from True)
eval (FALSE bm) = pure (bm.map_from False)
eval (In bm i is) = bm.map_from <$> liftA2 isMember (eval i) (eval is)
eval (Eq bm a b) = bm.map_from <$> liftA2 (==) (eval a) (eval b)
eval (Leq bm a b) = bm.map_from <$> liftA2 (<=) (eval a) (eval b)
eval (If c a b) = eval c >>= \c -> if c (eval a) (eval b)
eval (For id set expr) = eval set >>= \set -> for` id set expr
where
	for` :: Ident [Int] (Expression a) -> Sem a | TC a
	for` _ [] _ = fail "Cannot forloop over empty set"
	for` id [i] expr = store id i >>| eval expr
	for` id [i : is] expr = store id i >>| eval expr >>| for` id is expr
eval (:. a b) = eval a >>| eval b

Start = evalStateT (eval stmt2) 'Map'.newMap
where
	stmt1 :: Expression [Int]
	stmt1 =
		"x" =. New bimapId [1337 .. 1400]:.
		Variable "x" +. New bimapId [1 .. 2]

	stmt2 :: Expression Int
	stmt2 =
		"x" =. Elem bimapId 0:.
		For "i" (New bimapId [1 .. 5]) (
			"x" =. Variable "x" +. Elem bimapId 1
		)

// We did not get it working but we do think it is possible with some work under some conditions.
// 1. As mentioned in the assignment iTask doesn't work with types containing functions. To make this work we would have to omit the bimap. However, since the bimap is needed for the poor man's version of GADTs to work we would need to introduce it somewhere We could do it this by using a custom editor which applies a bimap to itself.
// 2. If we were to assume that the above was implemented, it must still be possible for generic functions to be derived for types with existential and universal quantifiers. We do not know if this is possible.
