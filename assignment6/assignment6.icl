/*
 * Erin van der Veen | s4431200
 * Oussama Danba | s4435591
 */
module assignment6

import iTasks
import StdDebug

// Data Structures
:: Question = {
		question :: String,
		answers  :: [String],
		correct  :: Int
	}

:: Result = Correct | Incorrect

derive class iTask Question, Result

// Start
Start :: !*World -> *World
Start world = startEngine (allTasks createUsers >>| login) world

// Functions
login :: Task ()
login = doAuthenticated (get currentUser >>- authTask) >>* [OnAllExceptions (\s -> displayError s >>| login)]

displayError :: String -> Task String
displayError message = viewInformation (Title "Error!") [] message

authTask :: User -> Task ()
authTask u
| isAdmin u = adminMode
| isTeacher u = teacherMode
| otherwise = studentMode
where
	isAdmin = \user -> isRole user "Admin"
	isTeacher = \user -> isRole user "Teacher"

isRole :: User String -> Bool
isRole (AuthenticatedUser _ roles _) role = isMember role roles

adminMode :: Task ()
adminMode = updateSharedInformation (Title "Edit questions") [] questions @ \_ -> ()

undef = undef

teacherMode :: Task ()
teacherMode = enterChoiceWithShared (Title "Choose an item to edit") [ChooseFromGrid snd] (mapRead addIndex questions) >>* [
		OnAction (Action "Append") (hasValue (showAndDo append)),
		OnAction (Action "Delete") (hasValue (showAndDo delete)),
		OnAction (Action "Edit") (hasValue (showAndDo edit)),
		OnAction (Action "First") (always (showAndDo append (-1, undef))),
		OnAction (Action "Clear") (hasValue (showAndDo clear)),
		OnAction (Action "Quit") (always (login))
	]
where
	showAndDo fun ip = viewSharedInformation "Current Questions" [] questions ||- fun ip
		>>* [
			OnValue (hasValue (\_ -> teacherMode)),
			OnAction (Action "Cancel") (always teacherMode)
		]

	append (i,_) = enterInformation "Add new item" [] >>=
			\n -> upd (\ps -> let (begin,end) = splitAt (i+1) ps in (begin ++ [n] ++ end)) questions

	delete (i, _) = upd (\qs -> removeAt i qs) questions

	edit (i, q) = updateInformation "Edit question" [] q >>= \q -> upd (\qs -> updateAt i q qs) questions

	clear (i, q) = enterInformation "Edit question" [] >>= \q -> upd (\qs -> updateAt i q qs) questions

studentMode :: Task ()
studentMode = get questions >>= \questions -> sequence "Enter Answer" (map enterAnswer questions) >>= showResults
where
	enterAnswer :: Question -> Task Result
	enterAnswer question = enterChoice ("Question:", question.question) [ChooseFromList snd] (addIndex question.answers) >>= \(i, _) -> return (if (i + 1 == question.correct) Correct Incorrect)

	showResults :: [Result] -> Task ()
	showResults rs = viewInformation (Title "Results") [] (("Correct: ", nrE Correct rs), ("Incorrect: ", nrE Incorrect rs)) @ \_ -> ()

	nrE :: Result [Result] -> Int
	nrE _ [] = 0
	nrE Correct [Correct : rs] = 1 + (nrE Correct rs)
	nrE Correct [_ : rs] = nrE Correct rs
	nrE Incorrect [Incorrect : rs] = 1 + (nrE Incorrect rs)
	nrE Incorrect [_ : rs] = nrE Incorrect rs



addIndex :: ([a] -> [(Int, a)])
addIndex = zip2 [0 ..]

questions :: Shared [Question]
questions = sharedStore "questions" []

:: MyAccount :== (String, String, String)

createUsers :: [Task UserAccount]
createUsers = map ((\u -> catchAll (createUser u) (\_ -> return u)) o toUserAccount) accounts
where
	accounts = [
			("Rinus", "sunir", "Teacher"),
			("Pieter", "reteip", "Teacher"),
			("Admin", "nimda", "Admin"),
			("Erin", "nire", "Student"),
			("Oussama", "amassuo", "Student"),
			("Abdellatif", "fitalledba", "Student"),
			("Frouke", "ekuorf", "Student")
		]

	toUserAccount :: MyAccount -> UserAccount
	toUserAccount (gebruikersnaam, wachtwoord, rol) = {
			credentials = {
					username = Username gebruikersnaam,
					password = Password wachtwoord
				},
			title = Nothing,
			roles = [rol]
		}


