/* Erin van der Veen | s4431200
 * Oussama Danba | s4435591
 */
module skeleton8

/*
  Advanved Progrmming 2017, Assignment 8
  Pieter Koopman, pieter@cs.ru.nl
*/

from iTasks import class iTask, class toPrompt, class Publishable, instance Publishable (Task a),
	instance toPrompt String, instance Functor Task,
	class TApplicative, instance TApplicative Task,
	generic gEq, generic gDefault, generic JSONDecode, generic JSONEncode, generic gText, generic gEditor, 
	:: JSONNode, :: TextFormat, :: Editor, :: TaskValue(..), :: Stability, :: Task, :: Action, 
	:: TaskCont(..), :: ViewOption(..), :: UpdateOption(..), :: EnterOption(..), :: Title(Title), instance toPrompt Title,
	-||-, -||, ||-, >>*, always, hasValue, updateInformation, enterInformation, viewInformation, startEngine
import qualified iTasks
import qualified iTasks.WF.Combinators.Overloaded as WF
import Data.Functor, Control.Applicative, Control.Monad
import Data.Tuple, StdClass, StdList, StdMaybe, StdString
import StdGeneric, StdBool
from StdFunc import o
import qualified Data.List as List
import qualified Data.Map as Map

from Control.Monad.State import :: StateT(StateT), gets, modify, runStateT, instance Functor (StateT s m), instance Applicative (StateT s m), instance Monad (StateT s m)
import Data.Error
from Data.Func import $
from Text import class Text(concat), instance Text String
from StdTuple import fst, snd

:: Expression
  = New      [Int]
  | Elem     Int
  | Variable Ident
  | Size     Set
  | (+.) infixl 6 Expression Expression
  | (-.) infixl 6 Expression Expression
  | (*.) infixl 7 Expression Expression
  | (=.) infixl 2 Ident Expression

:: Logical
  = TRUE | FALSE
  | (In) infix 4 Elem Set
  | (==.) infix 4 Expression Expression
  | (<=.) infix 4 Expression Expression
  | Not Logical
  | (||.) infixr 2 Logical Logical
  | (&&.) infixr 3 Logical Logical

:: Stmt
  = Logical Logical
  | If Logical Stmt Stmt
  | For Ident Set Stmt
  | Expression Expression


:: Result
  = RSet [Int]
  | RElem Int
  | RBool Bool

:: Set    :== Expression
:: Elem  :== Expression
:: Ident  :== String

// === State
:: Val = ValSet [Int]
       | ValElem Int

:: State :== 'Map'.Map Ident Val

// :: Sem Val :== StateT (('Map'.Map Ident Val) -> MaybeError String (Val, 'Map'.Map Ident Val))
:: Sem a :== StateT State (MaybeError String) a

derive class iTask Expression, Logical, Stmt, Result, Val

store :: Ident Val -> Sem Val
store id val = StateT (\s -> Ok (val, ('Map'.put id val) s))

read :: Ident -> Sem Val
read id = gets ('Map'.get id) >>= \val -> case val of
	Nothing = fail "Variable not found"
	Just v = pure v

fail :: String -> Sem a
fail msg = StateT (\_ -> Error msg)

// === semantics
eval :: Expression -> Sem Val
eval (New set) = pure (ValSet (removeDup set))
eval (Elem int) = pure (ValElem int)
eval (Variable id) = read id
eval (Size expr) = eval expr >>= \v -> case v of
	ValSet set -> pure (ValElem (length set))
	_ -> fail "Cannot get size of element"
eval (a +. b) = eval a >>= \va -> eval b >>= \vb -> valAdd va vb
eval (a -. b) = eval a >>= \va -> eval b >>= \vb -> valSub va vb
eval (a *. b) = eval a >>= \va -> eval b >>= \vb -> valMul va vb
eval (id =. expr) = eval expr >>= \b -> store id b

valAdd :: Val Val -> Sem Val
valAdd (ValElem a) (ValElem b) = pure $ ValElem (a + b)
valAdd (ValElem a) (ValSet b) = pure $ ValSet (removeDup [a : b])
valAdd (ValSet a) (ValElem b) = pure $ ValSet (removeDup [b : a])
valAdd (ValSet a) (ValSet b) = pure $ ValSet (removeDup (a ++ b))

valSub :: Val Val -> Sem Val
valSub (ValElem a) (ValElem b) = pure $ ValElem (a - b)
valSub (ValElem a) (ValSet b) = fail "Cannot subtract Set from Int"
valSub (ValSet a) (ValElem b) = pure $ ValSet (removeMember b a)
valSub (ValSet a) (ValSet b) = pure $ ValSet (removeMembers a b)

valMul :: Val Val -> Sem Val
valMul (ValElem a) (ValElem b) = pure $ ValElem (a * b)
valMul (ValElem a) (ValSet b) = pure $ ValSet (map ((*) a) b)
valMul (ValSet a) (ValElem b) = fail "Cannot multiply Int with Set"
valMul (ValSet a) (ValSet b) = pure $ ValSet [x \\ x <- b | isMember x a]

evalL :: Logical -> Sem Bool
evalL TRUE = pure True
evalL FALSE = pure False
evalL (elem In set) = eval elem >>= \ve -> eval set >>= \vs -> case (ve, vs) of 
	(ValElem e, ValSet s) = pure (isMember e s)
	_ = fail "Can only apply In on Int and Set"
evalL (a ==. b) = eval a >>= \va -> eval b >>= valEq va
evalL (a <=. b) = eval a >>= \va -> eval b >>= valLeq va
evalL (Not l) = evalL l >>= \vl -> pure $ (not vl)
// Lazy
evalL (a ||. b) = evalL a >>= \va -> case va of
	True = pure True
	False = evalL b
evalL (a &&. b) = evalL a >>= \va -> case va of
	False = pure False
	True = evalL b

valEq :: Val Val -> Sem Bool
valEq (ValElem a) (ValElem b) = pure $ a == b
valEq (ValSet a) (ValSet b) = pure $ isEmpty (removeMembers a b) && (isEmpty (removeMembers b a))
valEq _ _ = fail "Can only check equality on Int, Int and Set, Set"

valLeq :: Val Val -> Sem Bool
valLeq (ValElem a) (ValElem b) = pure $ a <= b
valLeq (ValSet a) (ValSet b) = pure $ isEmpty (removeMembers a b)
valLeq _ _ = fail "Can only check equality on Int, Int and Set, Set"

evalS :: Stmt -> Sem Result
evalS (If l a b) = evalL l >>= \vl -> if vl (evalS a) (evalS b)
evalS (For id set stmt) = eval set >>= \vset -> case vset of
	ValElem _ = fail "Cannot forloop over Element"
	ValSet vset = for` id vset stmt
where
	for` :: Ident [Int] Stmt -> Sem Result
	for` _ [] _ = fail "Cannot forloop over empty set"
	for` id [i] stmt = store id (ValElem i) >>| evalS stmt
	for` id [i : is] stmt = store id (ValElem i) >>| evalS stmt >>| for` id is stmt
evalS (Expression a) = eval a >>= \v -> case v of
	ValSet r = pure $ (RSet r)
	ValElem r = pure $ (RElem r)
evalS (Logical l) = evalL l >>= \r -> pure $ (RBool r)

// === printing
printString :: a -> String | print a
printString a = concat (print a [])

class print a :: a [String] -> [String]

instance print Int
where
	print i ss = [toString i : ss]

instance print String
where
	print s ss = [s : ss]

instance print [Int]
where
	print is ss = ["["] ++ ('List'.intersperse "," (map toString is)) ++ ["]" : ss]

instance print Bool
where
	print True ss = ["True" : ss]
	print False ss = ["False" : ss]

instance print Expression
where
	print (New is) ss = ["[" : print is ["]" : ss]]
	print (Elem i) ss = print i ss
	print (Variable id) ss = print id ss
	print (Size set) ss = ["Size " : print set ss]
	print (a +. b) ss = ["(" : print a [" +. " : print b [")" : ss]]]
	print (a -. b) ss = ["(" : print a [" -. " : print b [")" : ss]]]
	print (a *. b) ss = ["(" : print a [" *. " : print b [")" : ss]]]
	print (v =. e) ss = print v [" =. " : print e ss]

instance print Logical
where
	print TRUE ss = ["TRUE" : ss]
	print FALSE ss = ["FALSE" : ss]
	print (x In xs) ss = ["(" : print x [" In " : print xs [")" : ss]]]
	print (a ==. b) ss = ["(" : print a [" ==. " : print b [")" : ss]]]
	print (a <=. b) ss = ["(" : print a [" <=. " : print b [")" : ss]]]
	print (Not b)   ss = ["Not (" : print b [")" : ss]]
	print (a ||. b) ss = ["(" : print a [" ||. " : print b [")" : ss]]]
	print (a &&. b) ss = ["(" : print a [" &&. " : print b [")" : ss]]]

instance print Stmt
where
	print (If b t e) ss = ["If (" : print b [") then {\n" : print t ["\n} else {\n" : print e ["\n}\n" : ss]]]]
	print (For v s e) ss = ["For (" : print v [" : " : print s [") do {\n" : print e ["\n}\n" : ss]]]]
	print (Expression e) ss = print e ["\n" : ss]
	print (Logical l) ss = ["(" : print l [")" : ss]]

// === simulation
(>>>=)     :== 'iTasks'.tbind
(>>>|) a b :== 'iTasks'.tbind a (\_ -> b)
treturn    :== 'iTasks'.return
ActionOk   :== 'iTasks'.ActionOk
ActionQuit :== 'iTasks'.ActionQuit
ActionNew  :== 'iTasks'.ActionNew

defaultStmt :: Stmt
defaultStmt = If TRUE (Expression ("a" =. (Elem 1))) (Expression ("a" =. (Elem 2)))

Start w = startEngine (updateInformation (Title "Program") [] defaultStmt >>>= simulate) w

simulate :: Stmt -> Task Stmt
simulate stmt =
	(updateInformation (Title "Program") [] stmt
	-|| 'iTasks'.allTasks
	[ viewInformation (Title "String representation") [ViewAs printString] stmt
	, viewInformation (Title "Execution")             [ViewAs execute]       stmt
	]) >>>= simulate

where
	execute :: Stmt -> MaybeError String (String, [(String, String)])
	execute stmt = showExec (runStateT (evalS stmt) 'Map'.newMap)
	where
		showExec :: (MaybeError String (Result, State)) -> MaybeError String (String, [(String, String)])
		showExec res = case res of
			Ok (r, s) = Ok (showResult r, [(fst x, printVal (snd x)) \\ x <- 'Map'.toList s])
			Error e = Error e

	printVal :: Val -> String
	printVal (ValElem i) = toString i
	printVal (ValSet s)  = concat ('List'.intersperse ", " (map toString s))

	showResult :: Result -> String
	showResult (RElem i) = toString i
	showResult (RSet s)  = concat ('List'.intersperse ", " (map toString s))
	showResult (RBool b) = toString b
