/*
 * Erin van der Veen | s4431200
 * Oussama Danba | s4435591
 */
module serialize3Native

import StdEnv, StdMaybe, StdGeneric

instance == (Bin a) | == a where
  (==) Leaf Leaf = True
  (==) (Bin l a r) (Bin k b s) = l == k && a == b && r == s
  (==) _ _ = False

instance == Coin where
  (==) Head Head = True
  (==) Tail Tail = True
  (==) _    _    = False

:: Bin a = Leaf | Bin (Bin a) a (Bin a)
:: Coin = Head | Tail

class serialize a | read{|*|}, write{|*|} a

generic write a :: a [String] -> [String]
write{|Bool|} True ss = ["True": ss]
write{|Bool|} False ss = ["False": ss]
write{|Int|} i ss = [toString i : ss]
write{|UNIT|} UNIT ss = ss
write{|EITHER|} f g (LEFT x) ss = f x ss
write{|EITHER|} f g (RIGHT x) ss = g x ss
write{|PAIR|} f g (PAIR a b) ss = f a (g b ss)
write{|CONS of c|} f (CONS x) ss
| c.gcd_arity == 0 = [c.gcd_name : ss]
= ["(", c.gcd_name : f x [")" : ss]]
write{|OBJECT|} f (OBJECT x) s = f x s
write{|(,)|} f g (a, b) ss = ["(" : f a ["," : g b [")" : ss]]]

derive write [], Bin, Coin

generic read a :: [String] -> Maybe (a, [String])
read{|Bool|} ["True" : ss] = Just (True, ss)
read{|Bool|} ["False" : ss] = Just (False, ss)
read{|Bool|} _ = Nothing
read{|Int|} [s : ss] = Just (toInt s, ss)
read{|Int|} _ = Nothing
read{|UNIT|} ss = Just (UNIT, ss)
read{|EITHER|} f g ss = case f ss of
	Just (x, ss) = Just (LEFT x, ss)
	_ = case g ss of
		Just (x, ss) = Just (RIGHT x, ss)
		_ = Nothing
read{|PAIR|} f g ss = case f ss of
	Just (a, ss) = case g ss of
		Just (b, ss) = Just (PAIR a b, ss)
		_ = Nothing
	_ = Nothing
read{|CONS of d|} f ["(", c : ss]
| d.gcd_name == c = case f ss of
	Just (x, [")" : ss]) = Just (CONS x, ss)
	_ = Nothing
= Nothing
read{|CONS of d|} f [c : ss]
| d.gcd_name == c = case f ss of
	Just (x, ss) = Just (CONS x, ss)
	_ = Nothing
= Nothing
read{|OBJECT|} f ss = case f ss of
	Just (x, ss) = Just (OBJECT x, ss)
	_ = Nothing
read{|(,)|} f g ["(" : ss] = case f ss of
	Just (a, ["," : ss]) = case g ss of
		Just (b, [")" : ss]) = Just ((a, b), ss)
		_ = Nothing
	_ = Nothing
read{|(,)|} _ _ _ = Nothing

derive read [], Bin, Coin

Start =
  [test True
  ,test False
  ,test 0
  ,test 123
  ,test -36
  ,test [42]
  ,test [0..4]
  ,test [[True],[]]
  ,test [[[1]],[[2],[3,4]],[[]]]
  ,test (Bin Leaf True Leaf)
  ,test [Bin (Bin Leaf [1] Leaf) [2] (Bin Leaf [3] (Bin Leaf [4,5] Leaf))]
  ,test [Bin (Bin Leaf [1] Leaf) [2] (Bin Leaf [3] (Bin (Bin Leaf [4,5] Leaf) [6,7] (Bin Leaf [8,9] Leaf)))]
  ,test Head
  ,test Tail
  ,test (7,True)
  ,test (Head,(7,[Tail]))
  ,["End of the tests.\n"]
  ]

test :: a -> [String] | serialize, == a
test a =
  (if (isJust r)
    (if (fst jr == a)
      (if (isEmpty (tl (snd jr)))
        ["Oke"]
        ["Not all input is consumed! ":snd jr])
      ["Wrong result: ":write{|*|} (fst jr) []])
    ["read result is Nothing"]
  ) ++ [", write produces: ": s]
  where
    s = write{|*|} a ["\n"]
    r = read{|*|} s
    jr = fromJust r
