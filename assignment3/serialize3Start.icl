/*
 * Erin van der Veen | s4431200
 * Oussama Danba | s4435591
 */
module serialize3Start

/*
  Definitions for assignment 3 in AFP 2017
  Kind indexed generics
  Pieter Koopman, pieter@cs.ru.nl
  September 2017

  use environment: StdMaybe from Libraries/StdLib
*/

import StdEnv, StdMaybe

:: Write a :== a [String] -> [String]
:: Read a  :== [String] -> Maybe (a,[String])

// use this as serialize0 for kind *
class serialize a
where
	write :: a [String] -> [String]
	read  :: [String] -> Maybe (a,[String])

class serialize1 t
where
	write1 :: (Write a) (t a) [String] -> [String]
	read1  :: (Read a) [String] -> Maybe (t a, [String])

class serialize2 t
where
	write2 :: (Write a) (Write b) (t a b) [String] -> [String]
	read2  :: (Read a) (Read b) [String] -> Maybe (t a b, [String])

// ---

instance serialize Bool
where
	write b c = [toString b:c]
	read ["True":r] = Just (True,r)
	read ["False":r] = Just (False,r)
	read _ = Nothing

instance serialize Int
where
	write i c = [toString i:c]
	read [s:r]
	# i = toInt s
	| s == toString i
	  = Just (i,r)
	  = Nothing
	read _ = Nothing

// ---

:: UNIT     = UNIT
:: EITHER a b = LEFT a | RIGHT b
:: PAIR   a b = PAIR a b
:: CONS   a   = CONS String a

instance serialize UNIT
where
	write UNIT ss = ss
	read ss = Just (UNIT, ss)

instance serialize2 EITHER
where
	write2 wl _ (LEFT x) ss = wl x ss
	write2 _ wr (RIGHT x) ss = wr x ss
	read2 rl rr ss = case rl ss of
		Just (l, ss) = Just (LEFT l, ss)
		_ = case rr ss of
			Just (r, ss) = Just (RIGHT r, ss)
			_ = Nothing

instance serialize2 PAIR
where
	write2 wa wb (PAIR a b) ss = wa a (wb b ss)
	read2 ra rb ss = case ra ss of
		Just (a, ss) = case rb ss of
			Just (b, ss) = Just (PAIR a b, ss)
			_ = Nothing
		_ = Nothing

instance serialize1 CONS
where
	write1 w (CONS s x) ss = ["(" , s : w x [")" : ss]]
	read1 r ["(" , s : ss] = case r ss of
		Just (a, [")" : ss]) = Just (CONS s a, ss)
		_ = Nothing
	read1 _ _ = Nothing
// ---

:: ListG a :== EITHER (CONS UNIT) (CONS (PAIR a [a]))

fromList :: [a] -> ListG a
fromList []    = LEFT  (CONS NilString  UNIT)
fromList [a:x] = RIGHT (CONS ConsString (PAIR a x))

toList :: (ListG a) -> [a]
toList (LEFT  (CONS NilString  UNIT)) = []
toList (RIGHT (CONS ConsString (PAIR a x))) = [a:x]

NilString :== "Nil"
ConsString :== "Cons"

instance serialize [a] | serialize a
where
	write as ss = write2 (write1 write) (write1 (write2 write write)) (fromList as) ss
	read  ss   = case read2 (read1 read) (read1 (read2 read read)) ss of
		Just (as, ss) = Just (toList as, ss)
		_ = Nothing

// ---

:: Bin a = Leaf | Bin (Bin a) a (Bin a)

:: BinG a :== EITHER (CONS UNIT) (CONS (PAIR (Bin a) (PAIR a (Bin a))))

fromBin :: (Bin a) -> BinG a
fromBin Leaf = LEFT (CONS LeafString UNIT)
fromBin (Bin l a r) = RIGHT (CONS BinString (PAIR l (PAIR a r)))

toBin :: (BinG a) -> Bin a
toBin (LEFT (CONS _ UNIT)) = Leaf
toBin (RIGHT (CONS _ (PAIR l (PAIR a r)))) = Bin l a r

LeafString :== "Leaf"
BinString :== "Bin"

instance == (Bin a) | == a where
  (==) Leaf Leaf = True
  (==) (Bin l a r) (Bin k b s) = l == k && a == b && r == s
  (==) _ _ = False

instance serialize (Bin a) | serialize a
where
	write as ss = write2 (write1 write) (write1 (write2 write (write2 write write))) (fromBin as) ss
	read  ss   = case read2 (read1 read) (read1 (read2 read (read2 read read))) ss of
		Just (as, ss) = Just (toBin as, ss)
		_ = Nothing

// ---

:: Coin = Head | Tail
:: CoinG :== EITHER (CONS UNIT) (CONS UNIT)

fromCoin :: Coin -> CoinG
fromCoin Head = LEFT (CONS "Head" UNIT)
fromCoin Tail = RIGHT (CONS "Tail" UNIT)

toCoin :: CoinG -> Coin
toCoin (LEFT (CONS _ UNIT)) = Head
toCoin (RIGHT (CONS _ UNIT)) = Tail

instance == Coin where
  (==) Head Head = True
  (==) Tail Tail = True
  (==) _    _    = False

readCons :: String (Read a) [String] -> Maybe (CONS a, [String])
readCons x r ["(" , s : ss]
	| x == s = case r ss of
		Just (a, [")" : ss]) = Just (CONS s a, ss)
		_ = Nothing
	= Nothing
readCons _ _ _ = Nothing

instance serialize Coin where
	write c s = write2 (write1 write) (write1 write) (fromCoin c) s
	read    l = case read2 (readCons "Head" read) (readCons "Tail" read) l of
		Just (k, l) = Just (toCoin k, l)
		_ = Nothing

/*
	Define a special purpose version for this type that writes and reads
	the value (7,True) as ["(","7",",","True",")"]
*/
instance serialize (a,b) | serialize a & serialize b where
	write (a,b) c = ["("] ++ write a ([","] ++ write b [")" : c])
	read ["(" : c] = case read c of
		Just (a, ["," : c]) = case read c of
			Just (b, [")" : c]) = Just ((a, b), c)
			_ = Nothing
		_ = Nothing
	read _ = Nothing

// ---
// output looks nice if compiled with "Basic Values Only" for console in project options
Start = 
  [test True
  ,test False
  ,test 0
  ,test 123
  ,test -36
  ,test [42]
  ,test [0..4]
  ,test [[True],[]]
  ,test [[[1]],[[2],[3,4]],[[]]]
  ,test (Bin Leaf True Leaf)
  ,test [Bin (Bin Leaf [1] Leaf) [2] (Bin Leaf [3] (Bin Leaf [4,5] Leaf))]
  ,test [Bin (Bin Leaf [1] Leaf) [2] (Bin Leaf [3] (Bin (Bin Leaf [4,5] Leaf) [6,7] (Bin Leaf [8,9] Leaf)))]
  ,test Head
  ,test Tail
  ,test (7,True)
  ,test (Head,(7,[Tail]))
  ,["End of the tests.\n"]
  ]

test :: a -> [String] | serialize, == a
test a = 
  (if (isJust r)
    (if (fst jr == a)
      (if (isEmpty (tl (snd jr)))
        ["Oke"]
        ["Not all input is consumed! ":snd jr])
      ["Wrong result: ":write (fst jr) []])
    ["read result is Nothing"]
  ) ++ [", write produces: ": s]
  where
    s = write a ["\n"]
    r = read s
    jr = fromJust r
