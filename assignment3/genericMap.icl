/*
 * Erin van der Veen | s4431200
 * Oussama Danba | s4435591
 */
module genericMap

import StdEnv, StdGeneric, GenEq

generic gMap a b :: a -> b
gMap{|Int|}         x = x
gMap{|Real|}        x = x
gMap{|UNIT|}        x = x
gMap{|PAIR|}   f g (PAIR x y) = PAIR   (f x) (g y) 
gMap{|EITHER|} f g (LEFT x)   = LEFT   (f x)
gMap{|EITHER|} f g (RIGHT x)  = RIGHT  (g x)
gMap{|CONS|}   f   (CONS x)   = CONS   (f x)
gMap{|OBJECT|} f   (OBJECT x) = OBJECT (f x)

derive gMap Bin, [], (,)

:: Bin a = Leaf | Bin (Bin a) a (Bin a)
t = Bin (Bin Leaf 1 Leaf) 2 (Bin (Bin Leaf 3 Leaf) 4 Leaf)
l = [1..7]

fac :: Int -> Int
fac n
| n < 0 = prod [-1 .. n]
| n > 0 = prod [1 .. n]
= 0

// 1
//Start = gMap{|* -> *|} fac t
// (Bin (Bin Leaf 1 Leaf) 2 (Bin (Bin Leaf 6 Leaf) 24 Leaf))

//Start = gMap{|* -> *|} (\x -> (x, fac x)) l
// [(1,1),(2,2),(3,6),(4,24),(5,120),(6,720),(7,5040)]

//Start = gMap{|* -> * -> *|} (gMap{|* -> *|} fac) (gMap{|* -> *|} fac) (l,t)
// ([1,2,6,24,120,720,5040],(Bin (Bin Leaf 1 Leaf) 2 (Bin (Bin Leaf 6 Leaf) 24 Leaf)))

// 2
//Start = gEq{|*|} [1,2] [1,2]
// True

//Start = gEq{|*|} [1,2] [2,3]
// False

//Start = gEq{|* -> *|} (<) [1,2] [2,3]
// True
