/*
 * Erin van der Veen | s4431200
 * Oussama Danba | s4435591
 */
module assignment5

import iTasks

/*
	Pieter Koopman, pieter@cs.ru.nl
	Advanced Programming. Skeleton for assignment 5
 -	use this a project with environment iTasks
 -	executable must be in Examples/iTasks or a subdirectory
	You can also use the -sdk commandline flag to set the path
 -	check Project Options -> Profiling -> Dynamics to prevent recompilation
*/

:: Student =
	{ name :: String
	, snum :: Int
	, bama :: BaMa
	, year :: Int
	}

:: BaMa = Bachelor | Master

students :: [Student]
students =
	[{name = "Alice"
	 ,snum = 1000
	 ,bama = Master
	 ,year = 1
	 }
	,{name = "Bob"
	 ,snum = 1003
	 ,bama = Master
	 ,year = 1
	 }
	,{name = "Carol"
	 ,snum = 1024
	 ,bama = Master
	 ,year = 2
	 }
	,{name = "Dave"
	 ,snum = 2048
	 ,bama = Master
	 ,year = 1
	 }
	,{name = "Eve"
	 ,snum = 4096
	 ,bama = Master
	 ,year = 1
	 }
	,{name = "Frank"
	 ,snum = 1023
	 ,bama = Master
	 ,year = 1
	 }
	]

generic gToString a :: a -> String
gToString{|Int|} x = toString x
gToString{|String|} x = x
gToString{|UNIT|} _ = ""
gToString{|PAIR|} fx fy (PAIR x y) = fx x + " " + fy y
gToString{|EITHER|} fx _ (LEFT x) = fx x
gToString{|EITHER|} _ fy (RIGHT y) = fy y
gToString{|CONS of d|} fx (CONS x)
| d.gcd_arity == 0 = d.gcd_name
| otherwise = d.gcd_name + fx x
gToString{|OBJECT|} fx (OBJECT x) = fx x
gToString{|FIELD of f|} fx (FIELD x) = f.gfd_name + ": " + fx x
gToString{|RECORD|} fx (RECORD x) = fx x

instance + String where + s t = s +++ t

derive class iTask Student, BaMa
derive gToString Student, BaMa

Start w = startEngine [
	publish "/task1" (const task1),
	publish "/task2" (const task2),
	publish "/task3" (const task3),
	publish "/task4" (const task4),
	publish "/task5" (const task5),
	publish "/task6" (const task6),
	publish "/task7" (const task7),
	publish "/task8" (const task8)
	] w

task1 :: Task Student
task1 = enterInformation "Enter a new student" []

task2 :: Task [Student]
task2 = enterInformation "Enter new students" []

task3 :: Task Student
task3 = updateInformation "Update the student" [] (hd students)

task4 :: Task Student
task4 = enterChoice "Pick your favorite student" [] students

task5 :: Task Student
task5 = enterChoice "Pick your favorite student" [ChooseFromDropdown (\{Student | name} -> name)] students

task6 :: Task Student
task6 = enterChoice "Pick a student" [ChooseFromDropdown gToString{|*|}] students

task7 :: Task [Student]
task7 = enterMultipleChoice "Pick some partners" [ChooseFromCheckGroup (\s -> s.Student.name + ": " + gToString{|*|} s.Student.bama)] students

task8 :: Task Student
task8 = updateInformation ("Update Student: " + (hd students).Student.name) [UpdateAs (\s -> s.Student.name) (\s n -> {Student | s & name = n})] (hd students)
