/* Erin van der Veen | s4431200
 * Oussama Danba | s4435591
 */
module assignment9

// StdEnv
import StdBool
from StdClass import class Eq
from StdFunc import o, flip
import StdString
import StdOverloaded
import StdOrdList
from StdList import instance length [], repeatn, map, removeMember, instance == [a], isEmpty, removeMembers
import StdInt

// Platform <3
import Control.Applicative
import Control.Monad
import Control.Monad.State
import Data.Error
import Data.Functor
import qualified Data.List as List
import qualified Data.Map as Map
import Data.Maybe

// 1
// There is not really a good reason for chosing Dynamics over any of the others
// We had to pick one, and we decided to pick Dynamics
:: State_ :== 'Map'.Map Ident Dynamic
:: Ident :== String

// :: Sem a :== StateT (('Map'.Map Ident a) -> MaybeError String (a, 'Map'.Map Ident a))
:: Sem a :== StateT State_ (MaybeError String) a

// Definition was inspired by the first few serialize assignments
:: Show :== [String] -> [String]

store :: Ident a -> Sem a | TC a
store id val = StateT (\s -> Ok (val, ('Map'.put id (dynamic val) s)))

read :: Ident -> Sem a | TC a
read id = gets ('Map'.get id) >>= \val -> case val of
	Just (v :: a^) = pure v
	Just _ = fail "Given Type different from Expected Type"
	Nothing = fail "Variable not found"

fail :: String -> Sem a
fail msg = StateT (\_ -> Error msg)

// 2
:: Element :== Sem Int
:: Set :== Sem [Int]

class integer a :: Int -> a
class set a :: [Int] -> a
class size a b :: a -> b

instance integer Element where integer i = pure i
instance set Set where set is = pure is
instance size Set Element where size is = fmap length is

instance integer Show where integer i = \ss -> [toString i : ss]
instance set Show where set a = \ss -> [toString a : ss]
// :: ([String] -> [String]) -> ([String] -> [String])
instance size Show Show where size a = \ss -> ["size", "(" : a [")" : ss]]

instance + Element where + a b = liftA2 (+) a b
instance - Element where - a b = liftA2 (-) a b
instance * Element where * a b = liftA2 (*) a b

instance + Set where + a b = liftA2 'List'.union a b
instance - Set where - a b = liftA2 'List'.difference a b
instance * Set where * a b = liftA2 'List'.intersect a b

instance + Show where + a b = \ss -> ["(" : a [" + " : b [")" : ss]]]
instance - Show where - a b = \ss -> ["(" : a [" - " : b [")" : ss]]]
instance * Show where * a b = \ss -> ["(" : a [" * " : b [")" : ss]]]

// Binding strenth is copied from StdOverloaded to ensure expected behavior
class (+.) infixl 6 a b c :: a b -> c
class (-.) infixl 6 a b c :: a b -> c
class (*.) infixl 7 a b c :: a b -> c

instance +. Element Set Set where
	// "repeatn 1" to convert an element to a list
	(+.) a b = (+) (fmap (repeatn 1) a) b
instance *. Element Set Set where
	// liftA2 was obtained from cloogle using the query: :: (a b -> c) (m a) (m b) -> (m c)
	(*.) a b = liftA2 (map o (*)) a b

instance +. Set Element Set where
	(+.) a b = (+.) b a
instance -. Set Element Set where
	(-.) a b = liftA2 (flip removeMember) a b

instance +. Show Show Show where
	(+.) a b = \ss -> ["(" : a [" +. " : b [")" : ss]]]

instance -. Show Show Show where
	(-.) a b = \ss -> ["(" : a [" -. " : b [")" : ss]]]

eval :: (Sem a) -> MaybeError String a
eval sem = evalStateT sem 'Map'.newMap

// 3
// We have already defined most operations on Sets above.
// We just have to define the functions given in the assignment (variable and =.)
class Variable a where
	variable :: Ident -> a
	// 5 ensures that it has a weaker binding than the operations
	(=.) infix 5 :: Ident a -> a

instance Variable Element where
	variable id = read id
	(=.) id val = val >>= store id

instance Variable Set where
	variable id = read id
	(=.) id val = val >>= store id

instance Variable Show where
	variable id = \ss -> [id : ss]
	(=.) id val = \ss -> [id, " =. " : val ss]

// 4
// In order to implement the Statements, we must first implement the Logical operators
:: Condition :== Sem Bool

class Conditional a
where
	true :: a
	false :: a
	(&&.) :: a a -> a
	(||.) :: a a -> a
	(not`) :: a -> a

instance Conditional Condition
where
	true = pure True
	false = pure False
	(&&.) a b = liftA2 (&&) a b
	(||.) a b = liftA2 (||) a b
	(not`) a = fmap not a

instance Conditional Show
where
	true = \ss -> ["True" : ss]
	false = \ss -> ["False" : ss]
	(&&.) a b = \ss -> ["(" : a [" &&. " : b [")" : ss]]]
	(||.) a b = \ss -> ["(" : a [" ||. " : b [")" : ss]]]
	(not`) a = \ss -> ["not (" : a [")" : ss]]

class Equality a b where
	(==.) :: a a -> b
	(<=.) :: a a -> b

instance Equality Element Condition where
	(==.) a b = liftA2 (==) a b
	(<=.) a b = liftA2 (<=) a b

instance Equality Set Condition where
	(==.) a b = liftA2 (\a b -> sort a == sort b) a b
	(<=.) a b = liftA2 (\a b -> isEmpty (removeMembers a b)) a b

instance Equality Show Show where
	(==.) a b = \ss -> ["(" : a [" ==. " : b [")" : ss]]]
	(<=.) a b = \ss -> ["(" : a [" <=. " : b [")" : ss]]]

// Statements
class If a b :: a b b -> b
class For a b :: Ident a b -> b
class (:.) infixl 4 a b :: a b -> b

instance If Condition (Sem a) where
	If c a b = c >>= \condition = if condition a b

instance If Show Show where
	If c a b = \ss -> ["If (" : c [") {" : a ["} else {" : b ["}" : ss]]]]

instance For Set (Sem a) where
	For var set stmt = set >>= \vset -> for` var vset stmt
	where
		for` :: Ident [Int] (Sem a) -> Sem a
		for` _ [] _ = fail "Cannot forloop over empty set"
		for` id [i] stmt = store id i >>| stmt
		for` id [i : is] stmt = store id i >>| stmt >>| for` id is stmt

instance For Show Show where
	For var set stmt = \ss -> ["For ", var, " in " : set ["{" : stmt ["}" : ss]]]

instance :. (Sem a) (Sem b) where
	(:.) a b = a >>| b

instance :. Show Show where
	(:.) a b = \ss -> a ["; " : b ss]

// Printing
// We decided that using a class system was actually easier than modifying the state to contain the string view,
// because using the record means that we would have to implement the Monad ourselves.
instance toString [a] | toString a
where
	toString xs = "[" +++ toString` xs
	where
		toString` [x : []] = toString x +++ "]"
		toString` [x : xs] = toString x +++ ", " +++ toString` xs
		toString` [] = "]"

// Since we chose to use the serialize method from the first few assignments
// It does not make sense to define print as :: Sem a -> String
// As such, the signature is different
print :: ([String] -> [String]) -> String
print a = 'List'.foldr1 (\a s -> a +++ s) (a [])

Start = print (For "i" s ("a" =. i + integer 5) :. "b" =. integer 1337)
where
	i :: Show
	i = variable "i"

	s :: Show
	s = set [1, 2, 3]
//Start :: MaybeError String Int
//Start = eval (For "i" s ("a" =. i + integer 5) :. "b" =. integer 1337)
//where
//	i :: Element
//	i = variable "i"
//
//	s :: Set
//	s = set [1, 2, 3]
