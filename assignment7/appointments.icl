/*
 * Erin van der Veen | s4431200
 * Oussama Danba | s4435591
 */
module appointments

from StdFunc import seq

from Data.List import maximum
import System.Time

import iTasks
import iTasks.Extensions.DateTime

:: Appointment =
	{ title        :: String
	, when         :: DateTime
	, duration     :: Time
	, owner        :: User
	, participants :: [User]
	}

:: ProposedAppointment =
	{ appointment   :: Appointment
	, proposedTimes :: [(DateTime, [User])]
	, id            :: Int
	}

derive class iTask Appointment, ProposedAppointment

appointmentEndTime :: Appointment -> DateTime
appointmentEndTime ap
# beginTime = ap.when
# duration = ap.duration
= addDateTimeTime beginTime duration
where
	addDateTimeTime :: DateTime Time -> DateTime
	addDateTimeTime a t
	# (Timestamp tsa) = utcDateTimeToTimestamp a
	= timestampToGmDateTime (Timestamp (tsa + 3600 * t.Time.hour + 60 * t.Time.min + t.Time.sec))

appointments :: Shared [Appointment]
appointments = sharedStore "appointments"
	[ {title="Advanced Programming",  when={DateTime|year=2017,mon=11,day= 26,hour=10,min=30,sec=0}, duration={Time|hour=2,min=0,sec=0}, owner=AuthenticatedUser "root" ["admin","manager"] (Just "Root user"), participants=[]}
	, {title="Information Retrieval", when={DateTime|year=2017,mon=11,day= 26,hour=15,min=30,sec=0}, duration={Time|hour=2,min=0,sec=0}, owner=AuthenticatedUser "root" ["admin","manager"] (Just "Root user"), participants=[]}
	, {title="Cultural Contacts",     when={DateTime|year=2017,mon=11,day= 27,hour=13,min= 0,sec=0}, duration={Time|hour=2,min=0,sec=0}, owner=AuthenticatedUser "root" ["admin","manager"] (Just "Root user"), participants=[]}
	, {title="Historical Grammar",    when={DateTime|year=2017,mon=11,day= 27,hour=15,min= 0,sec=0}, duration={Time|hour=2,min=0,sec=0}, owner=AuthenticatedUser "root" ["admin","manager"] (Just "Root user"), participants=[]}
	, {title="Text Mining",           when={DateTime|year=2017,mon=11,day= 28,hour= 8,min=30,sec=0}, duration={Time|hour=2,min=0,sec=0}, owner=AuthenticatedUser "root" ["admin","manager"] (Just "Root user"), participants=[]}
	, {title="Testing Techniques",    when={DateTime|year=2017,mon=11,day= 28,hour= 8,min=30,sec=0}, duration={Time|hour=2,min=0,sec=0}, owner=AuthenticatedUser "root" ["admin","manager"] (Just "Root user"), participants=[]}
	, {title="Testing Techniques",    when={DateTime|year=2017,mon=11,day=20,hour= 8,min=30,sec=0}, duration={Time|hour=2,min=0,sec=0}, owner=AuthenticatedUser "root" ["admin","manager"] (Just "Root user"), participants=[]}
	, {title="Advanced Programming",  when={DateTime|year=2017,mon=11,day=20,hour=10,min=30,sec=0}, duration={Time|hour=2,min=0,sec=0}, owner=AuthenticatedUser "root" ["admin","manager"] (Just "Root user"), participants=[]}
	]

proposedAppointments :: Shared [ProposedAppointment]
proposedAppointments = sharedStore "proposedAppointments" []

isFutureAppointment :: DateTime Appointment -> Bool
isFutureAppointment time appointment = time <= appointment.when

addAppointment :: Appointment -> Task Appointment
addAppointment ap = appendTopLevelTaskFor SystemUser (createAttributes "" ap SystemUser) False (addAppointment` ap) @ \_ -> ap
where
	addAppointment` :: Appointment -> Task Appointment
	addAppointment` ap = upd (\aps -> [ap: aps]) appointments >>| allTasks (map (\user -> waitForDateTime ap.when >>| appendTopLevelTaskFor user (createAttributes "Appointment: " ap user) False (waitForDateTime (appointmentEndTime ap) >>| treturn ())) ap.participants) @ \_ -> ap

createAttributes :: String Appointment User -> TaskAttributes
createAttributes prefix ap user = workerAttributes user
	[ ("title",      prefix +++ ap.Appointment.title)
	, ("priority",   "Of the utmost importance")
	, ("createdBy",  toString ap.owner)
	, ("createdFor", toString user)
	]

addProposedAppointment :: ProposedAppointment -> Task ProposedAppointment
addProposedAppointment pap = upd (\paps -> [pap: paps]) proposedAppointments @ \_ -> pap

isRelevant :: User Appointment -> Bool
isRelevant u a = isMember u a.participants || u == a.owner

defaultAppointment :: User DateTime -> Appointment
defaultAppointment user time = {Appointment | title = "Appointment Title", when = time, duration = {Time | hour = 1, min = 0, sec = 0}, owner = user, participants = []}

getNextId :: [ProposedAppointment] -> Int
getNextId paps
#! ids = [x.ProposedAppointment.id \\ x <- paps]
= case ids of
	[] = 0
	_ = 1 + (maximum ids)

Start :: *World -> *World
Start w = startEngine (loginAndManageWorkList "Appointment Scheduler" worklist) w
where
	worklist :: [Workflow]
	worklist =
		[ restrictedTransientWorkflow "Show Appointments" "Appointments" [] showAppointmentsTask
		, restrictedTransientWorkflow "Make Appointment" "Add appointment" [] makeAppointmentTask
		, restrictedTransientWorkflow "Propose Appointment" "Propose Assignment" [] proposeAppointmentTask
		, restrictedTransientWorkflow "Manage Users" "Manage System Users" ["admin"] (forever manageUsers)
		]

showAppointmentsTask :: Task [Appointment]
showAppointmentsTask =
	get currentUser >>= \user ->
	get currentDateTime >>= \now ->
	viewSharedInformation "Appointments" [ViewAs (filter (\a -> isFutureAppointment now a && isRelevant user a))] appointments

makeAppointmentTask :: Task Appointment
makeAppointmentTask =
	get currentUser >>= \user ->
	get currentDateTime >>= \now ->
	let def = defaultAppointment user now in
	allTasks
		[ updateInformation "Title"    [] def.Appointment.title      @ \t a -> {Appointment | a & title=t}
		, updateInformation "When"     [] def.when                   @ \w a -> {a & when=w}
		, updateInformation "Duration" [] def.duration               @ \d a -> {a & duration=d}
		, enterMultipleChoiceWithShared "Participants" [ChooseFromCheckGroup id] users @ \ps a -> {a & participants=ps}
		]
	@ (\aps -> seq aps def) >>*
	[ OnAction (Action "Make") (hasValue (\a -> addAppointment a >>| makeAppointmentTask))
	, OnAction ActionCancel    (always makeAppointmentTask)
	]

proposeAppointmentTask :: Task ProposedAppointment
proposeAppointmentTask =
	get currentUser >>= \user ->
	get currentDateTime >>= \now ->
	defaultProposedAppointment user now >>= \def ->
	allTasks
		[ updateInformation "Title"    [] def.appointment.Appointment.title            @ \t p -> {p & appointment={Appointment | p.appointment & title=t}}
		, updateInformation "Duration" [] def.appointment.duration                     @ \d p -> {p & appointment={p.appointment & duration=d}}
		, enterMultipleChoiceWithShared "Participants" [ChooseFromCheckGroup id] users @ \ps p -> {p & appointment={p.appointment & participants=ps}}
		, updateInformation "Possible Times" [] [now]                                  @ \t p -> {p & proposedTimes=[(x, []) \\ x <- t]}
		]
	@ (\paps -> seq paps def) >>*
	[ OnAction (Action "Propose")  (hasValue (\a -> addProposedAppointment a >>= (\a -> availabilityCheckTask a >>|  proposeAppointmentTask)))
	, OnAction ActionCancel    (always proposeAppointmentTask)
	]
where
	defaultProposedAppointment :: User DateTime -> Task ProposedAppointment
	defaultProposedAppointment user time =
		get proposedAppointments >>= \paps ->
		let id = getNextId paps in
		treturn {ProposedAppointment | appointment = defaultAppointment user time, proposedTimes = [], id = id}

availabilityTask :: ProposedAppointment -> Task ProposedAppointment
availabilityTask pap = enterMultipleChoice "Times Available" [ChooseFromGrid fst] pap.proposedTimes >>= \pt -> updateAvailability pap.ProposedAppointment.id [fst x \\ x <- pt] @ \_ -> pap

updateAvailability :: Int [DateTime] -> Task ()
updateAvailability id proposedTimes = get currentUser >>= \user -> upd (availableAt id proposedTimes user) proposedAppointments @ \_ -> ()
where
	availableAt :: Int [DateTime] User [ProposedAppointment] -> [ProposedAppointment]
	availableAt _ _ _ [] = []
	availableAt id times user [p : ps]
	| id == p.ProposedAppointment.id = [{ProposedAppointment | p & proposedTimes = availableAt` p.proposedTimes times user} : availableAt id times user ps]
	| otherwise = [p : availableAt id times user ps]
	where
		availableAt` :: [(DateTime, [User])] [DateTime] User -> [(DateTime, [User])]
		availableAt` [] _ _ = []
		availableAt` [(dt, users) : dts] times user
		| isMember dt times = [(dt, [user: users]) : availableAt` dts times user]
		| otherwise = [(dt, users) : availableAt` dts times user]

availabilityOwnerTask :: ProposedAppointment [TaskId] -> Task ()
availabilityOwnerTask pap ids = viewSharedInformation "Proposed Appointment" [ViewAs (hd o filter (\pa -> pa.ProposedAppointment.id == pap.ProposedAppointment.id))] proposedAppointments @ (\paps -> (hd o (filter (\pa -> pa.ProposedAppointment.id == pap.ProposedAppointment.id))) paps) >>*
	[ OnAction (Action "Schedule")  (hasValue (\p -> pickFinalTimeTask p >>= addAppointment >>| treturn ()))
	, OnAction (Action "Delete")    (always (treturn ()))
	] >>= \_ -> removeTasks ids @ \_ -> ()
where
	removeTasks :: [TaskId] -> Task [()]
	removeTasks ids = allTasks (map (\id -> removeTask id topLevelTasks) ids)

pickFinalTimeTask :: ProposedAppointment -> Task Appointment
pickFinalTimeTask pap = enterChoice "Times Available" [ChooseFromGrid id] pap.proposedTimes @ \(time, _) -> {Appointment | pap.appointment & when = time}

availabilityCheckTask :: ProposedAppointment -> Task TaskId
availabilityCheckTask pap = allTasks (map (\user -> appendTopLevelTaskFor user (createAttributes "Availability for: " pap.appointment user) False (availabilityTask pap)) pap.appointment.participants) >>= \ids -> appendTopLevelTask (createAttributes "Schedule: " pap.appointment pap.appointment.owner) False (availabilityOwnerTask pap ids)
