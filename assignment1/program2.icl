/* Erin van der Veen | s4431200
 * Oussama Danba | s4435591
 */
// # Kinds #
// Bool :: *
// Bin :: * -> *
// Rose :: * -> *
// Bin Int :: *
// Tree :: * * -> *
// T1 :: (* -> *) * -> *
// T2 :: (* -> *) (* -> *) * -> *
// T3 :: (b c -> *) b c -> *
// T4 :: (b -> *) (c -> b) c -> *
// For the last two, we must know how the arguments are used within the types.
// Without that information we cannot determine the kind

module program2

import StdEnv

// # Type Constructor Classes #
class Container t where
	Cinsert :: a (t a) -> t a | < a
	Ccontains :: a (t a) -> Bool | <, Eq a
	Cshow :: (t a) -> [String] | toString a
	Cnew :: t a

:: Bin a = Leaf | Bin (Bin a) a (Bin a)

// Kinda disappointed that this is not allowed
/*
instance Container []
where
	Cinsert x [] = [x]
	Cinsert x yss=:[y : ys]
	| y < x = [y : Cinsert x ys]
	= [x : yss] 
	Ccontains = isMember
	Cshow = map toString
	Cnew = []
*/
instance Container []
where
	Cinsert x [] = [x]
	Cinsert x yss=:[y : ys]
	| y < x = [y : Cinsert x ys]
	= [x : yss] 
	Ccontains x xs = isMember x xs
	Cshow xs = map toString xs
	Cnew = []


instance Container Bin
where
	Cinsert x Leaf = Bin Leaf x Leaf
	Cinsert x (Bin l e r)
	| x < e = Bin (Cinsert x l) e r
	= Bin l e (Cinsert x r)
	Ccontains _ Leaf = False
	Ccontains x (Bin l e r)
	| x == e = True
	| x < e = Ccontains x l
	= Ccontains x r
	Cshow x = Cshow` x []
	where
		Cshow` :: (Bin a) [String] -> [String] | toString a
		Cshow` Leaf ss = ss
		Cshow` (Bin l e r) ss
		# ss = Cshow` r ss
		# ss = [toString e : ss]
		= Cshow` l ss
	Cnew = Leaf

Start = (Ccontains 3 c, Cshow c)
where
	c :: Bin Int
	c = foldr Cinsert Cnew [0..100]
