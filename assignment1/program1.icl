/* Erin van der Veen | s4431200
 * Oussama Danba | s4435591
 */
module program1

import StdEnv
import StdMaybe

:: Bin a = Leaf | Bin (Bin a) a (Bin a)
:: Rose a = Rose a [Rose a]


instance == (Bin a) | == a
where
	== Leaf Leaf = True
	== (Bin ll le lr) (Bin rl re rr) = ll == rl && le == re && lr == rr
	== _ _ = False

instance == (Rose a) | == a
where
	== (Rose x xs) (Rose y ys) = x == y && xs == ys

class serialize a
where
	write :: a [String] -> [String]
	read :: [String] -> Maybe (a, [String])

instance serialize Bool
where
	write b c = [toString b : c]
	read ["True" : r] = Just (True, r)
	read ["False" : r] = Just (False, r)
	read _ = Nothing

instance serialize Int
where
	write b c = [toString b : c]
	read [b : r]
	| all isDigit (fromString b) = Just (toInt b, r)
	| otherwise = Nothing

instance serialize String
where
	write b c = [b : c]
	read [s : ss] = Just (s, ss)
	read _ = Nothing

instance serialize [a] | serialize a
where
	write b c = ["[" : foldr write [] b ++ ["]" : c]]
	read ["[" : r] = readElems r
	where
		readElems :: [String] -> Maybe ([a], [String]) | serialize a
		readElems ["]" : ss] = Just ([], ss)
		// Would love to use the Maybe Monad, but it is not in StdEnv/StdLib
		readElems ss = case read ss of
			Just (e, s)
			# rest = readElems s
			| isNothing rest = Nothing
			# (el, ss) = fromJust rest
			= Just ([e : el], ss)
			Nothing = Nothing
	read _ = Nothing

instance serialize (Bin a) | serialize a
where
	write Leaf ss = write "Leaf" ss
	write (Bin l e r) ss = write "Bin" (write l (write e (write r ss)))
	read ["Leaf" : ss] = Just (Leaf, ss)
	read ["Bin" : ss]
	# l = read ss
	| isNothing l = Nothing
	# (l, ss) = fromJust l
	# e = read ss
	| isNothing e = Nothing
	# (e, ss) = fromJust e
	# r = read ss
	| isNothing r = Nothing
	# (r, ss) = fromJust r
	= Just (Bin l e r, ss)
	read _ = Nothing

instance serialize (Rose a) | serialize a
where
	// Include Constructor for consistency
	write (Rose e rs) ss = write "Rose" (write e (write rs ss))
	read ["Rose" : ss]
	# e = read ss
	| isNothing e = Nothing
	# (e, ss) = fromJust e
	# rs = read ss
	| isNothing rs = Nothing
	# (rs, ss) = fromJust rs
	= Just (Rose e rs, ss)
	read _ = Nothing

test :: a -> (Bool, [String]) | serialize,==a
test a = (isJust r && fst jr == a && isEmpty (tl (snd jr)), s)
where
	s = write a ["something"]
	r = read s
	jr = fromJust r

Start = [test True, test 37, test [12], test (Bin Leaf 12 (Bin (Bin Leaf 13 Leaf) 18564 (Bin Leaf 123 Leaf))), test (Rose "Hoi" [Rose "Doei" [], Rose "Nee" [Rose "Zeker wel" []]])]

