/*
 * Erin van der Veen | s4431200
 * Oussama Danba | s4435591
 */

module serialize2start

/* Review Questions
	1:
	Either would work. The definition :: UNIT = UNIT shows that a UNIT can ever
	only be a UNIT.
	instance == UNIT where (==) _ _ = TRUE
	
	2:
	If we assume that == a is correctly implemented, there is no reason to
	get the String. a ensures that x and y are of the same type and the == a
	should ensure that only equal constructors return TRUE.

	3:
	Leaf := CONS UNIT
	[] := CONS UNIT
	Leaf == [] should not even compile. == is defined as a a -> Bool, which
	cannot unify with Bin List -> Bool.
 */
/* Reflection
	This can definitely go wrong. The name of the constructor is not considered.
	Consider the start line that was added by us. It evaluates as Just ([], []),
	which shows that the name of the constructor should at least be considered
	when reading.
*/

/*
	Definition for assignment 2 in AFP 2017
	Pieter Koopman pieter@cs.ru.nl
	September 2017
 */

import StdEnv
import Control.Monad, Data.Maybe, Control.Applicative

class serialize a where
	write :: a [String] -> [String]
	read  :: [String] -> Maybe (a,[String])

instance serialize Bool where
	write b c = [toString b:c]
	read ["True":r]  = Just (True,r)
	read ["False":r] = Just (False,r)
	read _ = Nothing

instance serialize Int where
	write i c = [toString i:c]
	read [s:r]
	# i = toInt s
	| s == toString i
	= Just (i,r)
	= Nothing
	read _ = Nothing

instance serialize String
where
	write b c = [b : c]
	read [s : ss] = Just (s, ss)
	read _ = Nothing

// ---
writeP :: a [String] -> [String] | serialize a
writeP x c = "(" >/ x >/ ")" >/ c

readP :: [String] -> Maybe (a, [String]) | serialize a
readP ["(" : c] = case read c of
	Just (x, [")" : c]) = Just (x, c)
	_ = Nothing

// ---

:: UNIT       = UNIT
:: EITHER a b = LEFT a | RIGHT b
:: PAIR   a b = PAIR a b
:: CONS   a   = CONS String a

// == With Generic Information ==

/*
instance serialize UNIT
where
	write _ c = "UNIT" >/ c
	read ["UNIT": c] = Just (UNIT, c)
	read _ = Nothing

instance serialize (EITHER a b) | serialize a & serialize b
where
	write (LEFT a) c = "LEFT" >/ (writeP a c)
	write (RIGHT b) c = "RIGHT" >/ (writeP b c)
	read ["LEFT" : c] = readP c >>= \(x, c) -> Just (LEFT x, c)
	read ["RIGHT" : c] = readP c >>= \(x, c) -> Just (RIGHT x, c)

instance serialize (PAIR a b) | serialize a & serialize b
where
	write (PAIR a b) c = "PAIR" >/ (writeP a (writeP b c))
	read ["PAIR" : c] = readP c >>= \(x, c) -> readP c >>= \(y, c) -> Just (PAIR x y, c)

instance serialize (CONS a) | serialize a
where
	write (CONS s a) c = "CONS" >/ s >/ (writeP a c)
	read ["CONS", s : c] = readP c >>= \(x, c) -> Just (CONS s x, c)
 */

// == Without Generic Information ==

instance serialize UNIT
where
	write _ c = c
	read c = Just (UNIT, c)

// Without the extra information, this can only work if the types of LEFT and RIGHT are different
instance serialize (EITHER a b) | serialize a & serialize b
where
	write (LEFT a) c = a >/ c
	write (RIGHT b) c = b >/ c
	read c = case read c of
		Just (x, c) = Just (LEFT x, c)
		Nothing = case read c of
			Just (x, c) = Just (RIGHT x, c)
			_ = Nothing

instance serialize (PAIR a b) | serialize a & serialize b
where
	write (PAIR a b) c = a >/ b >/ c
	read c = read c >>= \(x, c) -> case read c of
		Just (y, c) = Just (PAIR x y, c)
		_ = Nothing

instance serialize (CONS a) | serialize a
where
	write (CONS s a) c = s >/ a >/ " " >/ c
	read [s : c] = case read c of
		Just (x, [" " : c]) = Just (CONS s x, c)
		_ = Nothing
	read _ = Nothing

// ---

:: ListG a :== EITHER (CONS UNIT) (CONS (PAIR a [a]))

(>/) infixr 1 :: a [String] -> [String] | serialize a
(>/) s c = write s c

fromList :: [a] -> ListG a
fromList [] = LEFT (CONS "Nil" UNIT)
fromList [x:xs] = RIGHT (CONS "Cons" (PAIR x xs))

toList :: (ListG a) -> [a]
toList (LEFT (CONS _ UNIT)) = []
toList (RIGHT (CONS _ (PAIR x xs))) = [x : xs]

instance serialize [a] | serialize a
where
	write xs c = (fromList xs) >/ c
	read c = read c >>= \(x, c) -> Just (toList x, c)

:: Bin a = Leaf | Bin (Bin a) a (Bin a)
:: BinG a :== EITHER (CONS UNIT) (CONS (PAIR (Bin a) (PAIR a (Bin a))))

fromBin :: (Bin a) -> BinG a
fromBin Leaf = LEFT (CONS "Leaf" UNIT)
fromBin (Bin l v r) = RIGHT (CONS "Bin" (PAIR l (PAIR v r)))

toBin :: (BinG a) -> Bin a
toBin (LEFT (CONS _ UNIT)) = Leaf
toBin (RIGHT (CONS _ (PAIR l (PAIR v r)))) = Bin l v r

instance serialize (Bin a) | serialize a
where
	write xs c = (fromBin xs) >/ c
	read c = read c >>= \(x, c) -> Just (toBin x, c)

instance == (Bin a) | == a where // better use the generic approach
	(==) Leaf Leaf = True
	(==) (Bin l a r) (Bin k b s) = l == k && a == b && r == s
	(==) _ _ = False

// ---

Start :: Maybe ([Bool], [String])
Start = read ["Hoi", " "]

/*
Start = [test True
	,test False
	,test 0
	,test 123
	,test -36
	,test [42]
	,test [0..4]
	,test [[True],[]]
	,test (Bin Leaf True Leaf)
	,test [Bin (Bin Leaf [1] Leaf) [2] (Bin Leaf [3] (Bin Leaf [4,5] Leaf))]
	,test [Bin (Bin Leaf [1] Leaf) [2] (Bin Leaf [3] (Bin (Bin Leaf [4,5] Leaf) [6,7] (Bin Leaf [8,9] Leaf)))]
	]
*/

test :: a -> ([String],[String]) | serialize, == a
test a =
	(if (isJust r)
	 (if (fst jr == a)
	  (if (isEmpty (tl (snd jr)))
	   ["Oke "]
	   ["Fail: not all input is consumed! ":snd jr])
	  ["Fail: Wrong result ":write (fst jr) []])
	 ["Fail: read result is Nothing "]
	 , ["write produces ": s]
	)
where
	s = write a ["\n"]
	r = read s
	jr = fromJust r
